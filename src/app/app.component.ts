import { Component } from "@angular/core";
import { fromEvent } from "rxjs";
import { map, startWith } from "rxjs/operators";

@Component({
  selector: "bank-mfe-menu-bottom-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  readonly show$ = fromEvent<CustomEvent<boolean>>(
    window,
    "bank-mfe-menu-bottom-show-menu"
  ).pipe(
    map((event) => event.detail),
    startWith(false)
  );
}
